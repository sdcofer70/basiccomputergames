These are short programs that are conversions of simple text-mode games found in the book "Basic Computer Games, Microcomputer Edition (1978)" [1].  I am not releasing these using any license since they are not my programs, even though they are conversions.

Each game is self-contained in its own source file and are compiled using a standard C compiler for .c files and FreeBASIC for the BASIC conversions.

My goal is to add everything found in the book, which may take time as I am converting them only occasionally.

Please note that there are barely any sanity checks on inputs and other things; if you want to be more "complete" and "secure" you're welcome to add such things yourself.  This is just a base no-frills conversion of the original programs.

[1] https://www.amazon.com/BASIC-Computer-Games-Microcomputer-David/dp/0894800523
