dim as integer kcacid, water, target, lives

print "Chemist"
print "Originally by Wayne Teeter, Ridgecrest, CA; adapted from Basic Computer Games, Microcomputer Edition (1978).  FreeBASIC version (c) 2018 Stephen D. Cofer.  No license."
print
print "The ficticious chemical kryptocyanic acid can only be diluted by the ratio of seven (7) parts water to three (3) parts acid.  If any other ratio is attempted, the acid becomes unstable and soon explodes.  Given the amount of acid, you must decide how much water to add for dilution.  If you miss, you face the consequences."
print
lives = 9
while (lives <> 0)
	kcacid = int (rnd(1)*50)
	target = 7 * kcacid / 3
	print kcacid; " liters of kryptocyanic acid.  How much water?"
	input water
	if (abs (target - water) > (target / 20)) then
		lives = lives - 1
		print "Sizzle!  You have just been desalinated into a blob of quivering protoplasm!  You have"; lives; " lives remaining."
		continue while
	else
		print "Good job!  You may breathe now, but don't inhale the fumes!"
	end if
wend
print "Your nine lives are used, but you will be long rememberd for your contributions to the field of comic book chemistry."

