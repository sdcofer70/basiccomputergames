#include <stdio.h>
#include <stdlib.h>
#include <time.h>


#define MAX_AMOUNT 50
#define VARIANCE 20.0
#define TARGET_AMOUNT(x) (7.0 * x / 3.0)

int main (void)
{
	float kcacid, target, water;
	int lives = 9;
	
	srand (time (NULL));
	printf ("Chemist\n\n");
	printf ("Originally by Wayne Teeter, Ridgecrest, CA; adapted from Basic Computer Games, Microcomputer Edition (1978).\n");
	printf ("C version by Stephen D. Cofer (2018).  No license.\n");
	printf ("\nThe ficticious chemical kryptocyanic acid can only be diluted by the ratio of seven (7) parts water\n");
	printf ("to three (3) parts acid.  If any other ratio is attempted, the acid becomes unstable and soon explodes.\n");
	printf ("Given the amount of acid, you must decide how much water to add for dilution.  If you miss, you face the consequences.\n\n");
	while (lives != 0)
	{
		kcacid = (float) (rand() % (MAX_AMOUNT - 1)) + 1;
		target = TARGET_AMOUNT(kcacid);
		printf ("%d liters of kryptocyanic acid.\nHow much water? ", (int) kcacid);
		scanf ("%f", &water);
		if (abs (target - water) > (target / VARIANCE))
		{
			printf ("Sizzle!  You have just been desalinated into a blob of quivering protoplasm!\nYou have %d lives remaining.\n", --lives);
		}
		else
			printf ("Good job!  You may breathe now, but don't inhale the fumes!\n");
	}
	printf ("\nYour nine lives are used, but you will be long remembered for your contributions ot the field\nof comic book chemistry.\n\n");
}


