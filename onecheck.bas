sub print_board (d() as integer)
	dim as integer x, y, node
	for x = 0 to 7
		for y = 0 to 7
			node = (x*8)+y+1
			if (node < 10) then print " ";
			print node; "["; d(x,y); " ]";
		next
		print
		print
	next
end sub

print "One Check - Solitaire Checker Puzzle"
print "Created by David Ahl; adapted from Basic Computer Games, Microcomputer Edition (1978)."
print "FreeBASIC version (c) 2018 Stephen D. Cofer.  No license."
print
print "40 checkers are placed on the 2 outside spaces of a standard 64-square checkerboard."
print "The object is to remove as many checkers as possible by diagonal jumps (as in standard checkers)."
print "Use the numbered board to indicate the square you wish to jump from and to.  On the board printed"
print "out on each turn ''[ 1 ]' indicates a checker and '[ 0 ]' an empty square.  When you have no"
print "possible jumps remaining, input a '0' in response to question 'Jump from?'"
print
dim as integer a(8,8), j, k, f, t, fx, fy, tx, ty, jumps, tleft
rem print "Here is the numerical board:"
rem print
for j = 0 to 7 
	for k = 0 to 7
		if (j >1 and j < 6) and (k > 1 and k < 6) then
			a(j,k)=0
		else
			a(j,k)=1
		end if
			
	next
next
print "Here is the opening position of the checkers:"
print_board (a())
jumps = 0
dim as boolean mloop = true
while (mloop = true)
	input "Jump from? ", f
	if (f < 0 or f > 64) then
		print "Enter a valid square."
		continue while
	end if
	if (f <> 0) then
		input "Jump to? ", t
		if (t < 1 or t > 64) then
			print "Enter a valid square."
			continue while
		end if
		f = f - 1
		t = t - 1
		fx = int (f/8)
		fy = f - (int (f/8)*8)
		tx = int (t/8)
		ty = t - (int (t/8)*8)
rem 		print fx; " "; fy; " "; tx; " "; ty; a(fx, fy); a(tx,ty); abs (fx-tx); abs (fy-ty)
		if ((abs (fx-tx) <> 2) or (abs (fy-ty) <> 2) or (a(fx, fy) <> 1) or (a(tx, ty) <> 0)) then
			print "Invalid move.  Try again."
			continue while
		end if
		a(fx, fy) = 0
		a(tx, ty) = 1
		a((fx+tx)/2, (fy+ty)/2) = 0
		print
		print_board (a())
		jumps = jumps + 1

	else
	mloop = false
	end if
wend
for j = 0 to 7
	for k = 0 to 7
		if (a(j,k) = 1) then tleft = tleft + 1
	next
next
print 
print "You made"; jumps; " jumps and left"; tleft; " pieces remaining on the board."
print "Thanks for playing."
end



	

	
