#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define START_MONEY 100
#define RANDOM_CARD ( rand() % 12)

int main (void)
{
	int player_money = START_MONEY;
	int current_bet = 0;
	const char *cards[13]={"2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace"};
	int low_card, high_card, target_card;
	
	srand (time (NULL));
	printf ("Acey Ducey\n\n");
	printf ("Originally by Bill Palmby, Prarie View, IL; adapted from Basic Computer Games, Microcomputer Edition (1976).\n");
	printf ("C version by Stephen D. Cofer (2020). No license.\n\n");
	printf ("Acey-ducey is played in the following manner:\n");
	printf ("The dealer (computer) deals two cards face up.\n");
	printf ("You have an option to bet or not bet depending on whether\n");
	printf ("or not you feel the card will have\n");
	printf ("a value between the first two.  For no bet, input 0.\n\n");
	while (player_money > 0)
	{

		printf ("\nYou now have $%i.\n", player_money);
		while (1)
		{
			low_card = RANDOM_CARD;
			high_card = RANDOM_CARD;
			if (high_card > low_card) break;
		}

		target_card = RANDOM_CARD;
		printf ("Here are your next two cards.\n");
		printf ("%s %s\n", cards[low_card], cards[high_card]);

		while (1)
		{
			printf ("What is your bet? \n");
			scanf ("%i", &current_bet);
			if (current_bet >= 0 && current_bet <= player_money) break;
			printf ("That's not a valid bet.  Try again.\n");
		}
		if (current_bet == 0) 
		{
			printf ("Chicken!\n");
		}
		else
		{
			printf ("The card is: %s\n", cards[target_card]);
			if (target_card > low_card && target_card < high_card)
			{
				printf ("You win!\n");
				player_money += current_bet;
			}
			else
			{
				printf ("You lose...\n");
				player_money -= current_bet;
			}
		}
	}
	printf ("\nSorry, friend, but you've lost all your money.\n");
	printf ("Hope you had fun!\n");
	return 0;
}


		
		



