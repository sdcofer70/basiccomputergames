/* One Check - Solitaire Checker Puzzle
   Adapted from "Basic Computer Games, Microcomputer Edition (1978), by David Ahl.
   C version by Stephen D. Cofer (2018), granted free to use with no license.
*/

#include <stdio.h>
#include <stdlib.h>
#define BOARD_WH 8

int board_matrix [BOARD_WH][BOARD_WH];

void print_board (void)
{
	int x,y;
	for (x=0; x<BOARD_WH; x++)
	{
		for (y=0;y<BOARD_WH; y++)
		{
			printf ("%2d[%d] ", x*8+(y+1), board_matrix[x][y]);
		}
		printf ("\n");
	}
}

void show_instructions (void)
{
	printf ("One Check - Solitaire Checker Puzzle\n");
	printf ("Created by David Ahl; adapted from Basic Computer Games, Microcomputer Edition (1978).\n");
	printf ("C version Stephen D. Cofer (2018).  No license.\n\n");
	printf ("40 checkers are placed on the 2 outside spaces of a standard 64-square checkerboard.\n");
	printf ("The object is to remove as many checkers as possible by diagonal jumps (as in standard checkers.\n");
	printf ("Use the numbered board to indicate the square you wish to jump from and to.  On the board printed\n");
	printf ("out on each turn [ 1 ] indicates a checker and [ 0 ] an empty square.  When you have no\n");
	printf ("possible jumps remaining, input a 0 in response the question 'Jump from?'.\n\n");
	return;
}

int main (void)
{
	int x,y;
	int jumps = 0;
	int left = 0;
	int move_from = 1;
	int move_to;
	int from_x, from_y, to_x, to_y;
	show_instructions();

	for (x=0;x<BOARD_WH; x++)
	{
		for (y=0;y<BOARD_WH; y++)
		{
			if ((x>1 && x < 6) && (y>1 && y<6))
				board_matrix[x][y] = 0;
			else
				board_matrix[x][y] = 1;
		}
	}
	printf ("Here is the opening position of the checkers:\n\n");
	print_board();
	
	while (1)
	{
		printf ("Jump from? ");
		scanf ("%d", &move_from);
		if (move_from < 0 || move_from > (BOARD_WH * BOARD_WH))
		{
			printf ("Enter a valid square.\n");
			continue;
		}
		if (move_from != 0)
		{
			printf ("Jump to? ");
			scanf ("%d", &move_to);
			if (move_to < 0 || move_to > (BOARD_WH * BOARD_WH))
			{
				printf ("Enter a valid square.\n");
				continue;
			}
			
			from_x = (int) --move_from / 8;
			from_y = move_from - (from_x * 8);
			to_x = (int) --move_to / 8;
			to_y = move_to - (to_x * 8);
			if ((abs(from_x - to_x) != 2) ||
			    (abs(from_y - to_y) != 2) ||
			    (board_matrix[from_x][from_y] != 1) ||
			    (board_matrix[to_x][to_y] != 0))
			{
				printf ("Invalid move.  Try again. \n");
				continue;
			}
			board_matrix [from_x][from_y] = 0;
			board_matrix [to_x][to_y] = 1;
			board_matrix [(from_x + to_x) / 2][(from_y + to_y) / 2] = 0;
			printf ("\n");
			print_board();
			jumps++;
		}
		else 
			break;
	}

	for (x=0; x< BOARD_WH; x++)
		for (y=0; y < BOARD_WH; y++)
			if (board_matrix [x][y] == 1) left++;

	printf ("\nYou made %d jumps and left %d pieces remaining on the board.\nThanks for playing.\n\n", jumps, left);
}


